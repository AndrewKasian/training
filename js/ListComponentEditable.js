(function($, window) {

    /**
     * Exposing ListComponentConfirm
     * @type {ListComponentEditable}
     */
    TRAINING.ListComponentEditable = ListComponentEditable;


    /**
     * ListComponentConfirm default options
     * @type {{Object}}
     * @private
     */
    ListComponentEditable.__defaultOptions = {
        deletePopupMessage: 'Are you sure you want to delete this?',
        editPopupMessage: 'Are you sure you want to edit this item?',
        acceptButton: 'Ok',
        cancelButton: 'No',
        itemSelector: null
    };

    function ListComponentEditable(element, options) {
        var self = TRAINING.Component(element, options);
        self.options = mergeDefaultOptions(options);
        self.actionItem = null;

        self.editPopup = TRAINING.PromptPopup(element.popupEdit,  {
            popupQuestion: self.options.editPopupMessage,
            triggeredSelector: self.options.triggeredEditSelector
        });

        console.log(self.editPopup);

        self.deletePopup = TRAINING.ConfirmPopup(element.popupDelete, {
            popupQuestion: self.options.deletePopupMessage,
            triggeredSelector: self.options.triggeredDeleteSelector
        });

        self.editPopup.__addEventListeners = addEventListener;
        self.editPopup.__addEventListeners();

        return self;

        function mergeDefaultOptions(options) {
            return $.extend({},ListComponentEditable.__defaultOptions, options);
        }

        function addEventListener() {

            self.deletePopup.on('popupOpen', function(data){
                var actionItem = getActionItem(data.triggerElem);
                makePopupQuestion(data.popup, self.deletePopup.options, actionItem);
            });

            self.deletePopup.on('confirmAccept', function(){
                self.actionItem.remove();
                console.info(self.actionItem.attr('data-name') + ' was deleted');
            });

            self.deletePopup.on('confirmDecline', function(){
                console.info('Deleting ' + self.actionItem.attr('data-name') + ' cancel');
            });

            self.editPopup.on('popupOpen', function(data){
                var actionItem = getActionItem(data.triggerElem);
                makePopupQuestion(data.popup, self.editPopup.options, actionItem);
            });

            self.editPopup.on('promptAccept', function(data){
                setItemText(data.inputValue);
                console.info(self.actionItem.attr('data-name') + ' was changed');
            });

            self.editPopup.on('promptDecline', function(){
                console.info('Editing ' + self.actionItem.attr('data-name') + ' decline');
            });
        }

        function getActionItem(triggerElem){
            if(self.options.itemSelector){
                return self.actionItem = $(triggerElem.closest(self.options.itemSelector));
            }else{
                return self.actionItem = triggerElem
            }
        }

        function setItemText(inputText){
            if(inputText){
                self.actionItem.attr('data-name',inputText);
                self.actionItem.find('span').text(inputText)
            }
        }

        function makePopupQuestion(popup, popupOptions, actionItem) {
            var popupQuestion = actionItem.attr('data-name') + ' - ' + popupOptions.popupQuestion;
            setPopupQuestion(popup, popupQuestion);
        }

        function setPopupQuestion(popup, popupQuestion) {
            popup.find('.confirm-text').text(popupQuestion);
        }
    }

}(jQuery, self));