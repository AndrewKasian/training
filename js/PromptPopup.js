(function ($, window){

    /**
     * Exposing PromptPopup
     * @type {PromptPopup}
     */
    TRAINING.PromptPopup = PromptPopup;

    /**
     * PromptPopup default options
     * @type {{Object}}
     * @private
     */
    PromptPopup.__defaultOptions = {};

    function PromptPopup(element, options){
        var self = TRAINING.ConfirmPopup(element, options);
        self.__addEventListeners = addEventListener;

        promptPopupInit();
        self.__addEventListeners();
        return self;

        function promptPopupInit(){
            var input = $('<input class="prompt-input" type="text"/>');
            input.insertAfter(self.element.find('.confirm-text'));
        }

        function addEventListener() {
            self.on('popupOpen', function(data){
                data.popup.find('input').val('');
            });

            self.on('confirmAccept', function(data){
                var inputValue = data.popup.find('input').val();
                self.trigger('promptAccept', {inputValue: inputValue, triggerElem: data.triggerElem});
            });

            self.on('confirmDecline', function(data){
               self.trigger('promptDecline', data)
            });
        }
    }

}(jQuery, self));