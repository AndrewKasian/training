$(function (){

   var deletePopup = $('.confirm-popup');
   var editPopup = $('.prompt-popup');
   TRAINING.ListComponentEditable({popupDelete: deletePopup, popupEdit: editPopup}, {
      triggeredDeleteSelector : '.item .delete-item',
      triggeredEditSelector: '.item .edit-item-value',
      itemSelector: '.item'
   });
});