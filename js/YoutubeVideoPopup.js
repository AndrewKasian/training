(function ($, window) {

    /**
     * Exposing YoutubeVideoPopup
     * @type {YoutubeVideoPopup}
     */
    TRAINING.YoutubeVideoPopup = YoutubeVideoPopup;

    /**
     * YoutubeVideoPopup default options
     * @type {{Object}}
     * @private
     */
    YoutubeVideoPopup.__defaultOptions = {

    };

    /**
     *
     * @param element
     * @param options
     * @returns {*}
     * @constructor
     */
    function YoutubeVideoPopup(element, options) {
        var self = TRAINING.Popup(element, options);
        var parentOpen = self.open;
        var parentClose = self.close;
        self.__youtubePlayer = null;
        self.__youtubeVideoID = self.element.data('video-id');

        self.open = open;
        self.close = close;
        self.__videoStart = videoStart;
        self.__videoStop = videoStop;

        youtubePlayerInit();
        return self;

        function open(){
            parentOpen();
            self.__videoStart();
        }

        function close() {
            parentClose();
            self.__videoStop();
        }

        function videoStart() {
            self.__youtubePlayer.playVideo();
            self.trigger('youtubeVideoStart');
        }

        function videoStop() {
            self.__youtubePlayer.stopVideo();
            self.trigger('youtubeVideoStop');
        }

        function youtubePlayerInit(){
            var playerDiv = document.createElement('div');
            self.element.prepend(playerDiv);

            TRAINING.YoutubeService.init().done(function(){
                self.__youtubePlayer = new YT.Player(playerDiv, {
                    height: '300px',
                    width: '100%',
                    videoId: self.__youtubeVideoID,
                    playerVars: {
                        autoplay: '0',
                        modestbranding: '1',
                        showinfo: '1',
                        color: 'white',
                        rel: '0',
                        wmode: 'transparent',
                        loop: '1',
                        start: '0'
                    }
                });
            });
        }
    }

}(jQuery, self));
