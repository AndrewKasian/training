(function ($, window) {

    /**
     * Exposing ConfirmPopup
     * @type {ConfirmPopup}
     */
    TRAINING.ConfirmPopup = ConfirmPopup;

    /**
     * ConfirmPopup default options
     * @type {{Object}}
     * @private
     */
    ConfirmPopup.__defaultOptions = {
        popupQuestion: 'OLOLOLOLO ?',
        acceptText: 'Yes',
        cancelText: 'Cancel'
    };

    function ConfirmPopup(element, options) {
        var self = {};
        self.options = mergeDefaultOptions(options);
        self = TRAINING.Popup(element, self.options);
        var parentClose = self.close;

        self.close = close;
        self.__addEventListeners = addEventListeners;

        confirmPopupInit();
        self.__addEventListeners();
        return self;

        function mergeDefaultOptions(options) {
            return $.extend({}, ConfirmPopup.__defaultOptions, options);
        }

        function confirmPopupInit(){
            var src = $('<span class="confirm-text">' + self.options.popupQuestion + '</span>' +
                        '<div class="confirm-controls">' +
                            '<button class="confirm">' + self.options.acceptText + '</button>' +
                            '<button class="cancel">' + self.options.cancelText  + '</button>' +
                        '</div>');
            self.element.append(src);
            setPopupTriggeredButtons(self.element);
        }

        function addEventListeners() {
            self.on('popupOpen', function(data){
                self.triggeredElem = data.triggerElem;
            });

            self.buttonConfirm.on('click', function() {
                self.trigger('confirmAccept', {popup: self.element, triggerElem: self.triggeredElem});
                self.close();
            });

            self.buttonCancel.on('click', function() {
                self.trigger('confirmDecline', {popup: self.element, triggerElem: self.triggeredElem});
                self.close();
            });
        }

        function setPopupTriggeredButtons(popup){
            self.buttonConfirm = popup.find('.confirm');
            self.buttonCancel = popup.find('.cancel');
        }

        function close(){
            parentClose();
        }

    }

}(jQuery, self));