(function ($, window) {

    /**
     * Exposing Component
     * @type {Component}
     */

    TRAINING.Component = Component;

    /**
     * Component default options
     * @type {{}}
     * @private
     */
    Component.__defaultOptions = {

    };

    /**
     *
     * @param element
     * @param options
     * @returns {{Object}|*}
     * @constructor
     */
    function Component(element, options) {
        var self,
            handlers;
        self = {};
        handlers = {};
        self.element = element;
        self.options = mergeDefaultOptions(options);

        self.on = on;
        self.trigger = trigger;

        return self;

        function mergeDefaultOptions(options) {
            return $.extend({}, Component.__defaultOptions, options);
        }

        function on(eventName, handler) {
            if (handlers[eventName] === undefined) {
                handlers[eventName] = {queue: []};
            }

            handlers[eventName].queue.push(handler);
        }

        function trigger(eventName, data) {
            if (handlers[eventName] === undefined) {
                return;
            }

            var items = handlers[eventName].queue;
            items.forEach(function(item) {
                item(data || {});
            });
        }
    }

}(jQuery, self));