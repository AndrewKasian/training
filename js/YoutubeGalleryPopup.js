(function ($, window) {

    /**
     * Exposing YoutubeGalleyPopup
     * @type {YoutubeGalleyPopup}
     */
    TRAINING.YoutubeGalleyPopup = YoutubeGalleyPopup;


    /**
     * YoutubeGalleyPopup default options
     * @type {{Object}}
     * @private
     */
    YoutubeGalleyPopup.__defaultOptions = {

    };


    /**
     *
     * @param element
     * @param options
     * @returns {*}
     * @constructor
     */
    function YoutubeGalleyPopup(element, options) {
        var self = TRAINING.YoutubeVideoPopup(element, options);
        self.videoGalleryItems = $(self.options.videoGalleryItems);
        self.__youtubeVideoID = self.videoGalleryItems.first().data('video-id');

        self.__addEventListeners = addEventListeners;

        self.__addEventListeners();
        return self;

        function addEventListeners () {
            self.videoGalleryItems.on('click', function(){
                var videoID = $(this).data('video-id');
                self.__youtubePlayer.loadVideoById(videoID);
            });
        }
    }

}(jQuery, self));