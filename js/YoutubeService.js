(function ($, window) {

    var initDeferred;

    TRAINING.YoutubeService =  {
        init: function (){
            if(!initDeferred){
                initDeferred = $.Deferred();
                this.createYoutubeApi();
                window.onYouTubeIframeAPIReady = function(){
                    initDeferred.resolve() ;
                };
                return initDeferred.promise();
            }else{
                return initDeferred.promise();
            }
        },
        createYoutubeApi: function () {
            var tagYouTubeApi = document.createElement('script');
            tagYouTubeApi.src = "https://www.youtube.com/iframe_api";
            var firstScriptTag = document.getElementsByTagName('script')[0];
            firstScriptTag.parentNode.insertBefore(tagYouTubeApi, firstScriptTag);
        }
    };

}(jQuery, self));
