(function ($, window) {

    /**
     * Exposing Popup
     * @type {Popup}
     */
    TRAINING.Popup = Popup;

    /**
     * Popup default options
     * @type {{Object}}
     * @private
     */
    Popup.__defaultOptions = {};

    /**
     *
     * @param element
     * @param options
     * @returns {Object}
     * @constructor
     */
    function Popup(element, options) {
        var self = TRAINING.Component(element, options);
        var closePopupElem;
        var overlay = $('.overlay');
        self.triggeredElem = findTriggerElement();

        self.open = open;
        self.close = close;
        self.__addEventListeners = addEventListeners;

        popupInit();
        self.__addEventListeners();
        return self;

        function popupInit(){
            var spanClose = $('<span class="closePopup">x</span>');
            self.element.append(spanClose);
            closePopupElem = self.element.find('.closePopup');

            if(window.location.hash == self.triggeredElem.attr('href')){
                self.open();
            }
        }

        function open(eventTarget) {
            self.element.addClass('show');
            overlay.addClass('active');
            self.trigger('popupOpen', {popup: self.element, triggerElem: eventTarget});
        }

        function close() {
            self.element.removeClass('show');
            overlay.removeClass('active');
            self.trigger('popupClose');
        }

        function addEventListeners() {
            self.triggeredElem.on('click', function (event) {
                self.open(event.target);
            });

            closePopupElem.on('click', function () {
                self.close();
            });

            overlay.on('click', function(){
                self.close();
            });
        }

        function findTriggerElement(){
           var elem = $(self.options.triggeredSelector);
           if(elem.attr('href')){
               for(var i = 0; i < elem.length; i++){
                   if(elem.eq(i).attr('href').slice(1) == self.element.data('popup-id')){
                       return elem.eq(i);
                   }
               }
           }else{
               return elem
           }
        }
    }

}(jQuery, self));